<?php

use yii\db\Migration;

/**
 * 发布需求表
 */
class m170902_191813_table_demand extends Migration
{
    public function safeUp()
    {
	  $tableOptions = null;
	  if ($this->db->driverName === 'mysql') {
		  // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
		  $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
	  }
	  $this->createTable('{{%demand}}', [
             'id' => $this->primaryKey(),
             'user_id'  => $this->integer()->notNull()->defaultValue(0)->comment('发布者用户ID'),
			 'type'     => $this->integer(1)->notNull()->defaultValue(0)->comment('类型'),
		     'title'    => $this->string(250)->notNull()->defaultValue("")->comment('需求标题'),
			 'info'     => $this->text()->comment('需求描述'),
		     'deposit'  => $this->decimal(10, 2)->defaultValue(0)->comment('压金'),
			 'price'    => $this->decimal(10, 2)->defaultValue(0)->comment('开的价格'),
		     'embracer' => $this->integer(10)->notNull()->defaultValue(0)->comment('接受者用户ID'),
		     'product_img' => $this->string(200)->notNull()->defaultValue("")->comment("设成品图片"),
		     'product'     => $this->string(200)->notNull()->defaultValue("")->comment("成品包"),
             'status'   => $this->smallInteger()->notNull()->defaultValue(0),
             'created_at' => $this->integer()->notNull(),
             'updated_at' => $this->integer()->notNull(),
         ], $tableOptions);
	  
	   $this->createTable('{{%demand_log}}', [
             'id' => $this->primaryKey(),
             'user_id'  => $this->integer()->notNull()->defaultValue(0)->comment('操作者ID'),
		     'demand_id' => $this->integer(10)->notNull()->defaultValue(0)->comment('发布需求ID'),
			 'info'     => $this->text()->comment('描述'),
             'created_at' => $this->integer()->notNull(),
             'updated_at' => $this->integer()->notNull(),
         ], $tableOptions);
	   //balance
	    $userTable = \common\models\User::tableName();
		$sql = "alter table {$userTable} ADD  COLUMN  `balance` decimal (10,2)  NOT  NULL DEFAULT  0 comment '帐户余额'";
		 $this->execute($sql);
		 
		$this->createTable('{{%balance_log}}', [
             'id' => $this->primaryKey(),
             'user_id'  => $this->integer()->notNull()->defaultValue(0)->comment('操作者ID'),
		     'type'     => $this->integer()->notNull()->defaultValue(0)->comment("日志类型"),
		     'original' =>  $this->decimal(10,2)->notNull()->defaultValue(0)->comment('原来的金额'),
		     'operators' => $this->smallInteger(4)->notNull()->defaultValue(0)->comment("操作符 加减乘除"),
		     'pice'      => $this->decimal(10, 2)->notNull()->defaultValue(0)->comment("要加减乘除 的金额"),
		     'results'   =>  $this->decimal(10, 2)->notNull()->defaultValue(0)->comment("加减乘除之后的金额"),
			 'info'     => $this->string()->comment('需求描述'),
             'created_at' => $this->integer()->notNull(),
             'updated_at' => $this->integer()->notNull(),
         ], $tableOptions); 

    }

    public function safeDown()
    {
	  $this->dropTable('{{%demand}}');
	  $this->dropTable('{{%demand_log}}');
	  $this->dropTable('{{%balance_log}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170902_191813_table_demand cannot be reverted.\n";

        return false;
    }
    */
}
