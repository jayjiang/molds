<?php

use yii\db\Migration;

class m170901_175636_table_user_info extends Migration
{
    public function safeUp()
    {
         $tableOptions = null;
         if ($this->db->driverName === 'mysql') {
             // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
             $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
         }
         //$sql = "ALTER TABLE  ADD field_name field_type;  ";
//         $this->alterColumn('{{%user}}','coin',"int(10) unsigned not null default 0 COMMENT '金币'");
//         $this->alterColumn('{{%user}}','level',"int(10) unsigned not null default 0 COMMENT '等级'");
//         $this->alterColumn('{{%user}}','score',"int(10) unsigned not null default 0 COMMENT '积分'");
         $userTable = \common\models\User::tableName();
        $sql ="alter table {$userTable} ADD  COLUMN  `coin` INT (10) unsigned NOT  NULL DEFAULT  0 comment '金币',
                ADD COLUMN  `level` int(1)  unsigned not null default 0 COMMENT '等级',
                ADD COLUMN  `score` int(1)  unsigned not null default 0 COMMENT '积分' ";
        $this->execute($sql);
         $this->createTable('{{%user_info}}', [
             'id' => $this->primaryKey(),
             'user_id'  => $this->integer()->notNull()->defaultValue(0)->comment('用户ID'),
             'sex'      => $this->smallInteger()->notNull()->defaultValue(0)->comment('0保密，1男，2女'),
             'birthday' => $this->string(20)->notNull()->defaultValue('')->comment('生日'),
             'nicname'  => $this->string(20)->notNull()->defaultValue('')->comment('真实姓名'),
             'mobile'   => $this->string(20)->notNull()->defaultValue('')->comment('手机号码'),
             'card'     => $this->string()->notNull()->unique()->defaultValue('')->comment('身份证号'),
             'card_img'  => $this->string()->notNull()->defaultValue('')->comment('身份证号图片'),
             'status'   => $this->smallInteger()->notNull()->defaultValue(10),
             'created_at' => $this->integer()->notNull(),
             'updated_at' => $this->integer()->notNull(),
         ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%user_info}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170901_175636_table_user_info cannot be reverted.\n";

        return false;
    }
    */
}
