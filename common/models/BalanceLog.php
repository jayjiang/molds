<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "m_balance_log".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property string $original
 * @property integer $operators
 * @property string $pice
 * @property string $results
 * @property string $info
 * @property integer $created_at
 * @property integer $updated_at
 */
class BalanceLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm_balance_log';
    }
	
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type', 'operators', 'created_at', 'updated_at'], 'integer'],
            [['original', 'pice', 'results'], 'number'],
            #[['created_at', 'updated_at'], 'required'],
            [['info'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'type' => 'Type',
            'original' => 'Original',
            'operators' => 'Operators',
            'pice' => 'Pice',
            'results' => 'Results',
            'info' => 'Info',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
