<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "m_demand".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property string $title
 * @property string $info
 * @property string $deposit
 * @property string $price
 * @property integer $embracer
 * @property string $product_img
 * @property string $product
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Demand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm_demand';
    }
	
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type', 'embracer', 'status', 'created_at', 'updated_at'], 'integer'],
            [['info'], 'string'],
            [['deposit', 'price'], 'number'],
           # [['created_at', 'updated_at'], 'required'],
            [['title'], 'string', 'max' => 250],
            [['product_img', 'product'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'type' => 'Type',
            'title' => 'Title',
            'info' => 'Info',
            'deposit' => 'Deposit',
            'price' => 'Price',
            'embracer' => 'Embracer',
            'product_img' => 'Product Img',
            'product' => 'Product',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
