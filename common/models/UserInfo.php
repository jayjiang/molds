<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "m_user_info".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $sex
 * @property string $birthday
 * @property string $nicname
 * @property string $mobile
 * @property string $card
 * @property string $card_img
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class UserInfo extends \yii\db\ActiveRecord
{
    public $sexArr =[
	  0 => '保密',
	  1 => '男',
	  2 => '女',
	];
  
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm_user_info';
    }
	
	public function sexName(){
	  return $this->sexArr[$this->sex];
	}
	
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sex', 'status', 'created_at', 'updated_at'], 'integer'],
           # [['created_at', 'updated_at'], 'required'],
            [['birthday', 'nicname', 'mobile'], 'string', 'max' => 20],
            [['card', 'card_img'], 'string', 'max' => 255],
            [['card'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => '用户ID',
            'sex' => '性别',
            'birthday' => '生日',
            'nicname' => '真实性名',
            'mobile' => '电话',
            'card' => '身份证号',
            'card_img' => 'Card Img',
            'status' => '状态',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
