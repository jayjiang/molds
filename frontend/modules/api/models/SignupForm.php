<?php
namespace frontend\modules\api\models;
use Yii;
use yii\base\Model;
use common\models\User;
use common\models\UserInfo;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $confrimPassword;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['confrimPassword','required'],
            ['confrimPassword','compare','compareAttribute'=>'password'],
        ];
    }

    public function attributeLabels(){
        return [
            'username'=>'帐号',
            'password'=>'密码',
            'confrimPassword'=> '确认密码',
            'email' => '邮箱'

        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
		$userInfo = new UserInfo();
		$transaction  = Yii::$app->db->beginTransaction(\yii\db\Transaction::READ_COMMITTED);
		try{
		  $user->username = $this->username;
		  $user->email = $this->email;
		  $user->setPassword($this->password);
		  $user->generateAuthKey();
		  $ures =  $user->save();
		  $userInfo->user_id=$user->id;
		  $uires = $userInfo->save();
		  if($ures && $uires){
			$transaction->commit();
			 return $user;
		  } else {
			 $transaction->rollBack();
			 return false;
		  }
		}catch (\Exception $e) {
		  $transaction->rollBack();
		  return false;
		}
		
    }
}
