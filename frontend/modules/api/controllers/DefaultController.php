<?php

namespace frontend\modules\api\controllers;
use frontend\models\SignupForm;
use frontend\modules\api\models\LoginForm;
use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use frontend\modules\api\components\ApiBaseController;

/**
 * Default controller for the `api` module
 */
class DefaultController extends ApiBaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $arr1=array("shu"=>111,"zhu"=>"hhhhh","1"=>"ffff");
        $c=implode("##",$arr1);
        echo $c;
        $model = new LoginForm();
        //return $this->renderJson(200,'2222');
        return $this->render('index',['model'=>$model]);
    }
    
    /**
     * 注册接口
     */
    public function actionReg(){
        $email = Yii::$app->request->post("email");
        $username = Yii::$app->request->post("username");
        $password = Yii::$app->request->post('password');
        $confirmPassowrd = Yii::$app->request->post('confirmPassword');
        $model = new SignupForm();
		$model->email = $email;
        $model->username = $username;
        $model->password = $password;
        if($model->validate() && $model->signup()){
            return $this->renderJson(0,'注册成功');
        }else{
            $errStr = $this->errorSummary($model);
            return $this->renderJson(201,'注册失败,'.$errStr);
        }
    }
    
    /**
     * 登陆接口
     */
    public function actionLogin(){
        $username = Yii::$app->request->post('username');
        $password  = Yii::$app->request->post("password");
        if(empty($username)){
           return $this->renderJson(101,'用户名不能为空');
        }
        if(empty($password)){
           return $this->renderJson(102,'密码不能为空');
        }
        $model = new LoginForm();
        $model->username = $username;
        $model->password = $password;
        if($model->validate()&&$model->login()){
          $user_id = Yii::$app->user->id;
          return $this->renderJson(103,'登陆成功',['id'=>$user_id,'username'=>$username]);
        }else{
            $errStr = $this->errorSummary($model);
            return $this->renderJson(103,'登陆失败,'.$errStr);
        }

    }
    
    /**
     * 获取TOKEN
     */
    public  function  actionToken(){
        $csrf = Yii::$app->request->getCsrfToken();
        return $this->renderJson(0,'TOKEN获取成功',['token'=>$csrf]);
    }
    
    
    
    
}
