<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\modules\api\controllers;
use Yii;
use frontend\modules\api\components\ApiBaseController;
use common\models\Demand;

/**
 * Description of DemandController
 *
 * @author SFB
 */
class DemandController extends ApiBaseController{
  //put your code here
  /**
   * 提需求 列表
   */
  public function actionList(){
	
	
  }
  
  /**
   * 发布需求
   */
  public function actionRelease(){
	$title = Yii::$app->request->post("title");
	$finishTime = Yii::$app->request->post("finishTime");
	$info = Yii::$app->request->post("info");
	$price = Yii::$app->request->post('price');
	
	$model = new Demand();
	$model->title = $title;
	$model->finish_time = $finishTime;
	$model->price = $price;
	$model->info = $info;
	if($model->validate() & $model->save()){
	   return $this->renderJson(0,'发布成功',['id'=>$model->id]);
	}else{
	  $errStr = $this->errorSummary($model);
	  return $this->renderJson(201,'发布失败,'.$errStr);
	}
  }
  
  
  
  
  
  
  
  
  
}
