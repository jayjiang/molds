<?php

/**
 * Created by PhpStorm.
 * User: SFB
 * Date: 2017/9/2
 * Time: 1:44
 */

namespace frontend\modules\api\controllers;
use Yii;
use frontend\modules\api\components\ApiBaseController;
use common\models\UserInfo;
use common\models\BalanceLog;
use common\models\Demand;
use common\models\User;
/**
 * 个人（企业） 中心控制器
 * Class UserController
 * @package frontend\modules\api\controllers
 */
class UserController extends ApiBaseController {

  public function actionInfo() {
	$user_id = Yii::$app->user->id;
    $info = UserInfo::find()->where("user_id={$user_id}")->one();
	$infoData = $info->toArray();
	return $this->renderJson(0,'获取成功',['info'=>$infoData]);
  }
  
  /**
   * 编辑资料
   */
  public function actionUpdated(){
	if(Yii::$app->request->isPost){
	  $user_id = Yii::$app->user->id;
	  $model = UserInfo::find()->where("user_id={$user_id}")->one();
	  if(empty($model)){
		$model = new UserInfo();
	  }
	  $model->sex = Yii::$app->request->post("sex");
	  $model->user_id = $user_id;
	  $model->birthday = Yii::$app->request->post("birthday");
	  $model->nicname  = Yii::$app->request->post("nicname");
	  $model->mobile   = Yii::$app->request->post("mobile");
	  $model->card     = Yii::$app->request->post("card");
	  if($model->save()){
		 return $this->renderJson(0,'保存成功！');
	  }else{
		$errStr = $this->errorSummary($model);
		return $this->renderJson(201,'编辑资料失败,'.$errStr);
	  }
	}else{
	  return $this->renderJson(100,'请求数据错误');
	}
	
  }
  
  /**
   * 用户余额帐单
   */
  public function actionBill(){
	$user_id = Yii::$app->user->id;
	$query =  BalanceLog::find()->where("user_id={$user_id} and type=0");
	$count = $query->count();
	$pageSize = empty(Yii::$app->request->post("pageSize"))?20:Yii::$app->request->post("pageSize");
	$page     = empty(Yii::$app->request->post("page"))?1:Yii::$app->request->post("page");
	$totalPage = ceil($count/$pageSize);//获取总页数
	$limit = $pageSize * ($page-1);
	$list = $query->limit($limit)->offset($pageSize)->orderBy("created_at desc")->all()->toArray();
	return $this->renderJson(0, '获取成功',['list'=> $list,'totalPage'=>$totalPage,'page'=>$page]);
  }
  
  /**
   * 获取-需求详细
   * @return type
   */
  public function acitonDemand(){
	$user_id = Yii::$app->user->id;
	$did = Yii::$app->request->post("did");
	$info = Demand::find()->where("id={$did} and user_id={$user_id}")->one()->toArray();
	if(empty($info)){
	  return $this->renderJson(1002, '获取失败');
	}
	return $this->renderJson(0,'获取成功', ['info'=>$info]);
  }
  
  /**
   * 修改密码
   */
  public function actionPwd(){
	$user_id = Yii::$app->user->id;
	$model = User::find()->where('id='.$user_id)->one();
	$oldPassword = Yii::$app->request->post('oldPassword');
	$newPassword = Yii::$app->request->post('newPassword');
	$confirmPassword = Yii::$app->request->post("confirmPassword");
	if(!$model->validatePassword($oldPassword)){
	   return $this->renderJson(10001,'原密码错误');
	}
	if($newPassword != $confirmPassword){
	  return $this->renderJson(1002,'两次新密码输入不一至');
	}
	$model->setPassword($newPassword);
	if($model->update()){
	   Yii::$app->user->logout();
	   return $this->renderJson(0, '密码修改成功，请重新登陆');
	}else{
	  return $this->renderJson(1003,'密码修改失败');
	}
	
	
  }
  
  
  
  

}
