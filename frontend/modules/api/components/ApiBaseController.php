<?php
namespace frontend\modules\api\components;
use Yii;
use yii\web\Controller;
use yii\helpers\Json;
class ApiBaseController extends  Controller{
    
    public $postData;
    public function beforeAction($action){
       $this->layout = false;
       $this->enableCsrfValidation=false;
       $this->postData = Yii::$app->request->post();
       if($action->controller->id != 'default'){
           if(Yii::$app->user->isGuest){
             echo  Json::encode(['code'=>403,'msg'=>'请先登陆','data'=>[],'status'=>200]);
             exit();
           }
       }
        if($action->id!="token"){
          //  $this->valiSign();
        }
       return  parent::beforeAction($action);
    }
    
    public function renderJson($code=0,$msg='',$data=[],$status=200){
        return  $this->asJson(['code'=>$code,'msg'=>$msg,'data'=>$data,'status'=>$status]);
    }

    /**
     *  参数 数组转化成URL
     */
    public function arrayToStrUelparams($arr){
         $tempArr=[];
         foreach($arr as $key=>$value){
            $tempArr[]=$key."=".$value;
         }
         return explode("&",$tempArr);
    }

    /**
     * 验证 sign
     * @param $arr
     */
    public function valiSign(){
       //接口sign验证
        $sign = Yii::$app->request->post("sign");
        if(empty($sign)){
            echo  Json::encode(['code'=>401,'msg'=>'验证错误','data'=>[],'status'=>200]);
            exit();
        }
        $data = Yii::$app->request->post();
        unset($data["sign"]);
        $arr=[];
        foreach($data as $key=>$value){
            $arr[]=$key."=".$value;
        }
        $return = implode("&",$arr);
        if(md5($return) == $sign){
            return true;
        }else{
            echo  Json::encode(['code'=>402,'msg'=>'验证错误','data'=>[],'status'=>200]);
            exit();
        }
    }

    public function errorSummary($model){
          $errormsg = "";
          foreach($model->errors as $key){
                $errormsg = $key[0].".";
          }
          return $errormsg;
    }
    
}